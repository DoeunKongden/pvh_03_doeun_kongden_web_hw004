import React, { Component } from "react";
import { Table } from "flowbite-react";
import Swal from "sweetalert2";
import 'animate.css';


export class TableComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            secondProp: this.props,
        }
    }

    changeButton = (id) => {
        this.props.statusChange(id)
    }

    showBox = (data) => {
        Swal.fire({
            html:
                'ID: ' + `${data.id}` +
                '<br/>' + 'Email: ' + `${data.userEmail}` + '<br/>' + 'Name: ' + `${data.userName}` + '<br/>' + 'Age: ' + `${data.userAge}`,
            
            showClass: {
                popup: 'animate__animated animate__bounceInLeft animate__faster'
            },
            hideClass: {
                popup: 'animate__animated animate__bounceOutRight animate__faster'
            }

        })
    }

    render() {
        return (
            <div className="min-w-[1240px] m-auto p-[50px]">
                <Table className="w-fit">
                    <Table.Head className="h-[70px]">

                        <Table.HeadCell className="bg-black text-white text-[20px]">
                            ID
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-black text-white text-[20px]">
                            EMAIL
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-black text-white text-[20px]">
                            USERNAME
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-black text-white text-[20px]">
                            AGE
                        </Table.HeadCell>
                        <Table.HeadCell className="bg-black text-white text-[20px]">
                            Action
                        </Table.HeadCell>
                    </Table.Head>
                    <Table.Body className="divide-y w-full ">
                        {this.props.userData.map((data, index) => (
                            <Table.Row key={index} className="even:bg-lime-300 bg-white">
                                <Table.Cell className="whitespace-nowrap font-medium text-black text-[15px]">
                                    {data.id}
                                </Table.Cell>
                                <Table.Cell className="text-black text-[15px]">
                                    {data.userEmail}
                                </Table.Cell>
                                <Table.Cell className="text-black">
                                    {data.userName}
                                </Table.Cell>
                                <Table.Cell className="text-black">
                                    {data.userAge}
                                </Table.Cell>
                                <Table.Cell className="flex gap-7">
                                    <button type="button" key={index} onClick={() => { this.changeButton(data.id) }} className={data.btn ? 'text-white bg-green-700 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 w-24' : 'w-24 text-white bg-gray-800 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 hover:bg-slate-200 hover:text-black'}>{data.btn ? "Done" : "Pending"}</button>
                                    <button type="button" onClick={() => { this.showBox(data) }} className="text-white bg-blue-700 hover:bg-blue-400 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2">Show More</button>
                                </Table.Cell>
                            </Table.Row>
                        ))}
                    </Table.Body>
                </Table>
            </div>
        )
    }
}

export default TableComponent;