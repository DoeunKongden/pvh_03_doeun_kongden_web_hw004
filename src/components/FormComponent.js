import React, { Component } from "react";
import { Button } from "flowbite-react";
import TableComponent from "./TableComponent";



export class FormComponent extends Component {

    constructor() {
        super()
        this.state = {
            userInfo: [
                { id: 1, userEmail: 'Kongden@gmail.com', userName: 'Kongden', userAge: 21, btn: false },
                { id: 2, userEmail: 'natsu@gmail.com', userName: 'Natsu', userAge: 20, btn: false }
            ],

            // For Storing userInput temporary 
            newUserEmail: 'null',
            newUserName: 'null',
            newUserAge: 'null',
            btn: false,
        }
    };


    handleChangeEmail = (values) => {
        this.setState({
            newUserEmail: values.target.value
        });
    }
    handleChangeName = (values) => {
        this.setState({
            newUserName: values.target.value
        })
    }
    handleChangeAge = (values) => {
        this.setState({
            newUserAge: values.target.value
        })
    }


    //function for update data in parent
    handleStatusChange = (id) => {
        // console.log(id);
        // this.setState((prevState)=>{   
        // }
        // )
        // const [newStatus] = newObject.filter(user => user.id === id)
        const notNewStatus = this.state.userInfo.filter(user => {
            if (user.id === id) {
                user.btn = !(user.btn)
                console.log("User Info : ",this.state.userInfo)
            }
            // console.log("1",this.state.userInfo);
            // console.log("2",user);
            return { ...this.state.userInfo }
        })

        // this.state = 1

        this.setState({
            userInfo: notNewStatus
        })
        console.log("notNewStatus, : ", notNewStatus);
        // console.log(this.state.userInfo.filter(user => user.id === id))
    }


    handleRegister = () => {
        // console.log(this.state.userInfo.length);
        const newObj = {
            id: this.state.userInfo.length + 1,
            userEmail: this.state.newUserEmail,
            userName: this.state.newUserName,
            userAge: this.state.newUserAge,
            btn: false,
        };

        //spread array and then put userinput in it
        this.setState({
            userInfo: [...this.state.userInfo, newObj], newUserEmail: "null", newUserName: "null", newUserAge: "null"
        }, () => console.log("New Array : ", this.state.userInfo));
    }

    render() {
        return (
            <div className="min-w-[1240px] m-auto p-4">
                <h1 className="w-full text-4xl text-center font-bold p-4 text-transparent bg-clip-text bg-gradient-to-r from-black to bg-indigo-300">Please Fill Your Information</h1>
                {/* Form Display */}
                <form className="max-w-[700px] m-auto">
                    <div className='grid grid-cols-1 gap-2'>
                        <input onChange={this.handleChangeEmail} className='border shadow-lg p-3' type="text" placeholder='✉️ Email' />
                        <input onChange={this.handleChangeName} className='border shadow-lg p-3' type="email" placeholder='👨‍💻 Username' />
                        <input onChange={this.handleChangeAge} className='border shadow-lg p-3' type="email" placeholder='🤓 Age' />
                    </div>

                    <Button onClick={this.handleRegister} color="dark" className="w-full mt-7 hover:bg-white hover:text-black hover:border-black" >
                        Register
                    </Button>
                </form>

                {/* Table Display */}
                <TableComponent userData={this.state.userInfo} statusChange={this.handleStatusChange} />
            </div>
        )
    }
}


export default FormComponent;