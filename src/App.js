import FormComponent from "./components/FormComponent";

function App() {
  return (
    <div className="App w-screen h-screen bg-[url('https://i.pinimg.com/originals/70/db/4e/70db4ed7b5ba7f157e8a57330889af79.gif')] bg-no-repeat bg-cover" >
      <FormComponent />
    </div>
  );
}

export default App;
